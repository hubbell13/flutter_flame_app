import 'package:flame/game.dart';
import 'package:flame/input.dart';

import 'player.dart';

class MyGame extends FlameGame with PanDetector {

  late final Player player;
  bool moving = false;

  @override
  Future<void> onLoad() async {
    await super.onLoad();

    player = Player();

    add(player);
  }

  void update(double dt) {
    super.update(dt);

    player.isMoving(false);
  }

  @override
  void onPanStart(DragStartInfo info) {
    moving = player.collide(info.raw.globalPosition.dx, info.raw.globalPosition.dy);
  }

  @override
  void onPanUpdate(DragUpdateInfo info) {

    if (moving && info.delta.game.x.abs() > 0.35) {
      player.move(Vector2(info.delta.game.x, 0));
      player.isMoving(true);
    } else {
      player.isMoving(false);
    }
  }

  @override
  void onPanEnd(DragEndInfo info) {
    moving = false;
    player.isMoving(false);
  }
}